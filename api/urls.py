from django.conf.urls import url
from api import views as api_views

urlpatterns = [
    url('login/$',
        api_views.Backend_ObtainAuthToken.as_view()
        ),  # 后台登录
    url('enterprise_auth/$',
        api_views.Enterprise_ObtainAuthToken.as_view()
        ),  # 企业登录
    url('employee_auth/$',
        api_views.Employee_ObtainAuthToken.as_view()
        ),  # 企业员工登录
    url('logout/$',
        api_views.Logout.as_view()
        ),  # 退出
    url('backend_logininfo',
        api_views.Backend_LoginInfo.as_view()
        ),  # 后台登录职员信息
    url('enterprise_logininfo',
        api_views.Enterprise_LoginInfo.as_view()
        ),  # 企业登录信息
    url('employee_logininfo',
        api_views.Employee_LoginInfo.as_view()
        ),  # 企业员工登录信息
    url('upload_images/$',
        api_views.UploadImage.as_view()
        ),  # 上传图片
    url('staff_member/$',
        api_views.StaffMember_ListCreateView.as_view()
        ),  # 职员列表 创建
    url('staff_member/(?P<pk>[0-9]+)/$',
        api_views.StaffMember_RetrieveUpdateView.as_view()
        ),  # 职员更新
    url('area/$',
        api_views.Area_ListCreateView.as_view()
        ),  # 地区列表 创建
    url('area/(?P<pk>[0-9]+)/$',
        api_views.Area_RetrieveUpdateView.as_view()
        ),  # 地区更新
    url('area_all/$',
        api_views.Area_AllListView.as_view()
        ),  # 所有地区列表
    url('household/$',
        api_views.Household_ListCreateView.as_view()
        ),  # 户口列表 创建
    url('household_all/$',
        api_views.Household_AllListView.as_view()
        ),  # 所有地区列表
    url('household/(?P<pk>[0-9]+)/$',
        api_views.Household_RetrieveUpdateView.as_view()
        ),  # 户口更新
    url('social_security/$',
        api_views.SocialSecurity_ListCreateView.as_view()
        ),  # 社保列表 创建
    url('social_security/(?P<pk>[0-9]+)/$',
        api_views.SocialSecurity_RetrieveUpdateView.as_view()
        ),  # 社保更新
    url('provident_fund/$',
        api_views.ProvidentFund_ListCreateView.as_view()
        ),  # 公积金列表 创建
    url('provident_fund/(?P<pk>[0-9]+)/$',
        api_views.ProvidentFund_RetrieveUpdateView.as_view()
        ),  # 公积金更新
    url('contract/$',
        api_views.Contract_ListCreateView.as_view()
        ),  # 合同列表 创建
    url('contract_all/$',
        api_views.Contract_All_ListView.as_view()
        ),  # 合同全部列表
    url('contract/(?P<pk>[0-9]+)/$',
        api_views.Contract_RetrieveUpdateView.as_view()
        ),  # 合同更新
    url('enterprise_all/$',
        api_views.Enterprise_AllListView.as_view()
        ),  # 所有企业列表
    url('enterprise_create/$',
        api_views.Enterprise_CreateView.as_view()
        ),  # 企业创建
    url('enterprise/$',
        api_views.Enterprise_ListView.as_view()
        ),  # 企业列表
    url('enterprise/(?P<pk>[0-9]+)/$',
        api_views.Enterprise_RetrieveUpdateView.as_view()
        ),  # 企业更新
    url('employee_all/$',
        api_views.Employee_AllListView.as_view()
        ),  # 企业员工全部列表
    url('employee/$',
        api_views.Employee_ListCreateView.as_view()
        ),  # 企业员工列表 创建
    url('employee/(?P<pk>[0-9]+)/$',
        api_views.Employee_RetrieveUpdateView.as_view()
        ),  # 企业员工更新
    url('changepwd/$',
        api_views.Change_Pwd.as_view()
        ),  # 企业员工修改密码
    url('contract_manage/$',
        api_views.ContractManage_ListCreateView.as_view()
        ),  # 前台合同列表 创建
    url('contract_manage/(?P<pk>[0-9]+)/$',
        api_views.ContractManage_RetrieveUpdateView.as_view()
        ),  # 前台合同更新
    url('contract_manage_pdf/(?P<pk>[0-9]+)/$',
        api_views.ContractManage_PDFView.as_view()
        ),  # 合同PDF预览
    url('sms_verification_code/(?P<pk>[0-9]+)/$',
        api_views.SMS_VerificationCode.as_view()
        ),  # 短信验证接口
]
