import re
import json

from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.authtoken.serializers import AuthTokenSerializer

from api.models import (
    StaffMember, Area, Household, SocialSecurity, ProvidentFund,
    Contract, Enterprise, Employee, ContractManage, ContractManageStatistics
)

from lxml.html.clean import Cleaner
from django.utils.timezone import datetime

PASSWORD_DEFAULT = '12345678'


class Base_AuthTokenSerializer(AuthTokenSerializer):
    """登录权限验证"""

    related_name = ''

    def validate(self, attrs):
        attrs = super(Base_AuthTokenSerializer, self).validate(attrs)
        if getattr(attrs['user'], self.related_name):
            return attrs
        else:
            msg = _('Unable to log in with provided credentials.')
            raise serializers.ValidationError(msg, code='authorization')


class Backend_AuthTokenSerializer(Base_AuthTokenSerializer):
    """后台登录权限验证"""
    related_name = 'staff'


class Enterprise_AuthTokenSerializer(AuthTokenSerializer):
    """企业登录权限验证"""
    related_name = 'business'


class Employee_AuthTokenSerializer(AuthTokenSerializer):
    """企业员工登录权限验证"""
    related_name = 'account'


class StaffMemberSerializer(serializers.ModelSerializer):
    """职员"""
    username = serializers.CharField(source='user.username', required=False)
    password = serializers.CharField(source='user.password', write_only=True, required=False)
    is_active = serializers.NullBooleanField(source='user.is_active', write_only=True, required=False)

    class Meta:
        model = StaffMember
        exclude = ('user',)

    def create(self, validated_data):
        user_data = validated_data.pop('user', None)
        username = user_data.pop('username', None)
        password = user_data.pop('password', None)

        # 判断用户名是否存在
        if User.objects.filter(username=username):
            raise serializers.ValidationError('username exists')

        if username and password:
            user = User.objects.create_user(
                username=username,
            )
            # 设置密码
            user.set_password(password)
            user.save()
            validated_data.update({'user': user})
            try:
                return super(StaffMemberSerializer, self).create(validated_data)
            except:
                user.delete()
                raise serializers.ValidationError('attribute error')
        else:
            raise serializers.ValidationError('need username and password')

    def update(self, instance, validated_data):

        if validated_data.get('user', None):

            user_data = validated_data.pop('user', None)
            username = user_data.pop('username', None)
            password = user_data.pop('password', None)
            is_active = user_data.pop('is_active', None)

            if username:
                if username != instance.user.username:
                    if User.objects.filter(username=username):
                        raise serializers.ValidationError('username exists')

                instance.user.username = username
            if password:
                instance.user.set_password(password)
            if (is_active is True) or (is_active is False):
                instance.user.is_active = is_active

        instance = super(StaffMemberSerializer, self).update(instance, validated_data)
        instance.user.save()
        return instance


class AreaSerializer(serializers.ModelSerializer):
    """地区"""

    class Meta:
        model = Area
        fields = '__all__'
        extra_kwargs = {
            'name':
                {'required': False}
        }


class AreaAllSerializer(serializers.ModelSerializer):
    """所有地区列表"""

    class Meta:
        model = Area
        fields = ('id', 'name')


class HouseholdSerializer(serializers.ModelSerializer):
    """户口"""

    class Meta:
        model = Household
        fields = '__all__'
        extra_kwargs = {
            'name':
                {'required': False}
        }


class HouseholdAllSerializer(serializers.ModelSerializer):
    """所有户口列表"""

    class Meta:
        model = Household
        fields = ('id', 'name')


class SocialSecuritySerializer(serializers.ModelSerializer):
    """社保"""

    class Meta:
        model = SocialSecurity
        fields = '__all__'
        extra_kwargs = {
            'name':
                {'required': False},
            'area':
                {'required': False},
        }


class ProvidentFundSerializer(serializers.ModelSerializer):
    """公积金"""

    class Meta:
        model = ProvidentFund
        fields = '__all__'
        extra_kwargs = {
            'area':
                {'required': False},
            'name':
                {'required': False},
        }


class ContractListSerializer(serializers.ModelSerializer):
    """合同列表"""

    class Meta:
        model = Contract
        fields = '__all__'
        extra_kwargs = {
            'stencil':
                {'read_only': False,
                 'required': False},
            'area':
                {'required': False},
            'title':
                {'required': False},
        }


class Contract_ALL_ListSerializer(serializers.ModelSerializer):
    """合同全部列表"""

    class Meta:
        model = Contract
        fields = ('id', 'title')


class ContractSerializer(serializers.ModelSerializer):
    """合同"""

    class Meta:
        model = Contract
        fields = '__all__'
        extra_kwargs = {
            'area':
                {'required': False},
            'title':
                {'required': False},
            'variable':
                {'read_only': True}
        }

    def create_variable(self, validated_data):
        # 获取文本内容
        if validated_data.get('content', None):
            cleaner = Cleaner(remove_tags=['span'])
            content = cleaner.clean_html(validated_data.get('content'))
            # 判断两个|| 获取变量名
            para = re.compile(r'[|](.*?)[|]', re.S)
            var = re.findall(para,
                             content)
            # 匹配变量并标注序号
            for n, v in enumerate(var):
                content = content.replace('|%s|' % v, 'variable_%s' % n)

            validated_data.update(
                {
                    'variable': {'variable_%s' % n: v for n, v in enumerate(var)},
                    'stencil': content
                }
            )
        return validated_data

    def create(self, validated_data):
        validated_data = self.create_variable(validated_data)
        return super(ContractSerializer, self).create(validated_data)

    def update(self, instance, validated_data):
        validated_data = self.create_variable(validated_data)
        return super(ContractSerializer, self).update(instance, validated_data)


class EnterpriseAllSerializer(serializers.ModelSerializer):
    """企业全部列表"""

    class Meta:
        model = Enterprise
        fields = ('id', 'full_name')


class EnterpriseSerializer(serializers.ModelSerializer):
    """企业"""
    username = serializers.CharField(source='user.username', required=False)
    password = serializers.CharField(source='user.password', write_only=True, required=False)
    is_active = serializers.NullBooleanField(source='user.is_active', write_only=True, required=False)

    class Meta:
        model = Enterprise
        exclude = ('user',)

    def create(self, validated_data):
        user_data = validated_data.pop('user', None)
        username = user_data.pop('username', None)
        password = user_data.pop('password', None)

        # 判断用户名是否存在
        if User.objects.filter(username=username):
            raise serializers.ValidationError('username exists')

        if username and password:
            user = User.objects.create_user(
                username=username,
            )
            # 设置密码
            user.set_password(password)
            user.save()
            validated_data.update({'user': user})
            try:
                return super(EnterpriseSerializer, self).create(validated_data)
            except:
                user.delete()
                raise serializers.ValidationError('attribute error')
        else:
            raise serializers.ValidationError('need username and password')

    def update(self, instance, validated_data):

        if validated_data.get('user', None):

            user_data = validated_data.pop('user', None)
            username = user_data.pop('username', None)
            password = user_data.pop('password', None)
            is_active = user_data.pop('is_active', None)

            if username:
                if username != instance.user.username:
                    if User.objects.filter(username=username):
                        raise serializers.ValidationError('username exists')

                instance.user.username = username
            if password:
                instance.user.set_password(password)
            if (is_active is True) or (is_active is False):
                instance.user.is_active = is_active

        instance = super(EnterpriseSerializer, self).update(instance, validated_data)
        instance.user.save()
        return instance

class EmployeeAllSerializer(serializers.ModelSerializer):
    """企业员工全部列表"""

    class Meta:
        model = Employee
        fields = ('id', 'actual_name')

class EmployeeSerializer(serializers.ModelSerializer):
    """企业员工"""
    enterprise_name = serializers.CharField(source='enterprise.full_name', read_only=True)
    username = serializers.CharField(source='user.username', required=False)
    password = serializers.CharField(source='user.password', write_only=True, required=False)
    is_active = serializers.NullBooleanField(source='user.is_active', write_only=True, required=False)

    class Meta:
        model = Employee
        exclude = ('user',)
        extra_kwargs = {
            'enterprise':
                {'required': False},
            'area':
                {'required': False},
            'household':
                {'required': False},
        }

    def create(self, validated_data):
        user_data = validated_data.pop('user', None)
        username = user_data.pop('username', None)
        if user_data.get('password', None):
            password = user_data.pop('password', None)
        else:
            password = PASSWORD_DEFAULT

        # 判断用户名是否存在
        if User.objects.filter(username=username):
            raise serializers.ValidationError('username exists')

        if username and password:
            user = User.objects.create_user(
                username=username,
            )
            # 设置密码
            user.set_password(password)
            user.save()
            validated_data.update({'user': user})
            try:
                return super(EmployeeSerializer, self).create(validated_data)
            except:
                user.delete()
                raise serializers.ValidationError('attribute error')
        else:
            raise serializers.ValidationError('need username and password')

    def update(self, instance, validated_data):

        if validated_data.get('user', None):

            user_data = validated_data.pop('user', None)
            username = user_data.pop('username', None)
            password = user_data.pop('password', None)
            is_active = user_data.pop('is_active', None)

            if username:
                if username != instance.user.username:
                    if User.objects.filter(username=username):
                        raise serializers.ValidationError('username exists')

                instance.user.username = username
            # 修改密码
            if password:
                instance.user.set_password(password)
                validated_data.update({'pwd_change': True})
            # 修改激活状态
            if (is_active is True) or (is_active is False):
                instance.user.is_active = is_active

        instance = super(EmployeeSerializer, self).update(instance, validated_data)
        instance.user.save()
        return instance


class ContractManageSerializer(serializers.ModelSerializer):
    """合同管理"""

    employee_name = serializers.CharField(source='employee.actual_name', required=False)
    contract_title = serializers.CharField(source='contract.title', required=False)

    class Meta:
        model = ContractManage
        fields = '__all__'
        extra_kwargs = {
            'contract':
                {'required': False},
            'employee':
                {'required': False},
            'generate':
                {'required': False,
                 'read_only': False},
        }

    def create(self, validated_data):

        # 获取参数 和 合同
        if validated_data.get('parameter', None) and validated_data.get('contract', None):
            contract = validated_data.get('contract')
            parameter = json.loads(validated_data.get('parameter'))
            stencil = contract.stencil
            # 替换文本中的变量
            for k, v in parameter.items():
                stencil = stencil.replace(k, v)
            validated_data.update({
                'generate': stencil
            })

        # 编号生成
        today = datetime.today()
        # 获取今天统计
        cms = ContractManageStatistics.objects.filter(date=today)
        if not cms:
            ContractManageStatistics.objects.create()
            cms = ContractManageStatistics.objects.filter(date=today)

        today_str = today.date().strftime('%Y%m%d')
        # 将统计数加一
        nums = cms.first().nums + 1
        nums_str = str(nums)
        # 如果统计数小于10000 做字符串前补零
        if nums < 10000:
            numbering = today_str + '0' * (5 - len(nums_str)) + nums_str
        else:
            numbering = today_str + str(nums_str)
        cms.update(nums=cms.first().nums + 1)
        validated_data.update({'numbering': numbering})

        return super(ContractManageSerializer, self).create(validated_data)

    def update(self, instance, validated_data):

        # 合同签署功能
        if validated_data.get('status') == '已签署':
            if validated_data.get('vcode', None) is None:
                raise serializers.ValidationError('需要短信验证码进行验证!')
            else:
                # 判断合同状态是否签署
                if instance.status == '已签署':
                    raise serializers.ValidationError('合同已签署, 请误重新签署该合同')
                if validated_data.get('vcode') != instance.vcode:
                    raise serializers.ValidationError('验证码有误!')
                # 验证码验证成功 将验证码清空
                else:
                    validated_data.update(
                        {'vcode': None, 'status': '已签署'}
                    )

        # 合同文本生成功能
        if validated_data.get('parameter', None):
            parameter = json.loads(validated_data.get('parameter'))
            stencil = instance.contract.stencil

            # 替换文本中的变量
            for k, v in parameter.items():
                stencil = stencil.replace(k, v)
            validated_data.update({
                'generate': stencil
            })
        return super(ContractManageSerializer, self).update(instance, validated_data)
