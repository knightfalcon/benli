from django.db import models
from django.contrib.auth.models import User, UserManager


# Create your models here.

class StaffMember(models.Model):
    """工作人员 Model"""
    # 职位选择项
    POSITION_CHOICE = (
        (0, '业务员'),
        (1, '经办人'),
        (2, '管理员')
    )

    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='staff')
    phone = models.CharField(max_length=11, verbose_name='电话', null=True)
    actual_name = models.CharField(max_length=20, verbose_name='姓名', null=True)
    position = models.IntegerField(choices=POSITION_CHOICE, default=0, verbose_name='职位', null=True)

    objects = UserManager()


class Area(models.Model):
    """地区 Model"""
    name = models.CharField(max_length=100, verbose_name='名称')
    min_insured_salary = models.DecimalField(decimal_places=4, max_digits=10, default=0, verbose_name='最低参保工资', null=True)
    max_insured_salary = models.DecimalField(decimal_places=4, max_digits=10,default=0, verbose_name='最高参保工资', null=True)
    service_fee = models.DecimalField(decimal_places=4, max_digits=10, verbose_name='服务费', null=True)
    is_active = models.BooleanField(default=True, verbose_name='激活')

    objects = models.Manager()


class Household(models.Model):
    """户口 Model"""
    name = models.CharField(max_length=100, verbose_name='名称')
    is_active = models.BooleanField(default=True, verbose_name='激活')

    objects = models.Manager()


class SocialSecurity(models.Model):
    """社保 Model"""
    area = models.ForeignKey(Area, verbose_name='地区', on_delete=models.CASCADE)
    name = models.CharField(max_length=100, verbose_name='名称')
    min_insured_salary = models.DecimalField(decimal_places=4, max_digits=10, default=0, verbose_name='最低参保工资', null=True)
    per_contribution_ratio = models.FloatField(default=0, verbose_name='个人缴纳比例', null=True)
    cor_payment_ratio = models.FloatField(default=0, verbose_name='企业缴纳比例', null=True)
    is_active = models.BooleanField(default=True, verbose_name='激活')

    objects = models.Manager()


class ProvidentFund(models.Model):
    """公积金 Model"""
    area = models.ForeignKey(Area, verbose_name='地区', on_delete=models.CASCADE)
    name = models.CharField(max_length=100, verbose_name='名称')
    min_insured_salary = models.DecimalField(decimal_places=4, max_digits=10, default=0, verbose_name='最低参保工资', null=True)
    per_contribution_ratio = models.DecimalField(decimal_places=4, max_digits=10, default=0, verbose_name='个人缴纳比例', null=True)
    cor_payment_ratio = models.DecimalField(decimal_places=4, max_digits=10, default=0, verbose_name='企业缴纳比例', null=True)
    is_active = models.BooleanField(default=True, verbose_name='激活')

    objects = models.Manager()


class Contract(models.Model):
    """合同 Model"""
    area = models.ForeignKey(Area, verbose_name='地区', on_delete=models.CASCADE)
    title = models.CharField(max_length=500, verbose_name='标题')
    content = models.TextField(blank=True, verbose_name='正文')
    stencil = models.TextField(blank=True, verbose_name='模版')
    variable = models.TextField(blank=True, verbose_name='变量')
    is_active = models.BooleanField(default=True, verbose_name='激活')

    objects = models.Manager()


class Enterprise(models.Model):
    """企业 Model"""

    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='business')
    full_name = models.CharField(max_length=200, verbose_name='企业全称呼', null=True)
    contact = models.CharField(max_length=20, verbose_name='联系人', null=True)
    contact_number = models.CharField(max_length=11, verbose_name='联系电话', null=True)
    license_number = models.CharField(max_length=50, verbose_name='企业营业执照号', null=True)
    tax_number = models.CharField(max_length=50, verbose_name='企业税号', null=True)
    business_license = models.CharField(max_length=500, verbose_name='营业执照', null=True)

    objects = UserManager()


class Employee(models.Model):
    """企业员工 Model"""

    enterprise = models.ForeignKey(Enterprise, on_delete=models.CASCADE, verbose_name='归属公司')
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='account')
    pwd_change = models.BooleanField(default=False, verbose_name='密码是否修改')
    area = models.ForeignKey(Area, on_delete=models.CASCADE, verbose_name='参保地区')
    actual_name = models.CharField(max_length=20, verbose_name='姓名', null=True)
    household = models.ForeignKey(Household, on_delete=models.CASCADE, verbose_name='户口性质')
    IDCard_number = models.CharField(max_length=100, verbose_name='身份证号码', null=True)
    phone = models.CharField(max_length=11, verbose_name='电话', null=True)
    eme_contract = models.CharField(max_length=50, verbose_name='紧急联系人', null=True)
    eme_phone = models.CharField(max_length=11, verbose_name='紧急联系人电话', null=True)
    IDCard_front = models.CharField(max_length=500, verbose_name='身份证正面', null=True)
    IDCard_back = models.CharField(max_length=500, verbose_name='身份证背面', null=True)
    buy_social_security = models.BooleanField(default=False, verbose_name='购买社保')
    buy_provident_fund = models.BooleanField(default=False, verbose_name='购买公积金')
    contract_manage_per = models.BooleanField(default=False, verbose_name='合同管理')
    social_security_manage_per = models.BooleanField(default=False, verbose_name='社保管理')
    purchase_at_min_wage = models.BooleanField(default=False, verbose_name='按最低合同工资购买')

    objects = UserManager()


class ContractManage(models.Model):
    """合同管理 Model"""
    numbering = models.CharField(max_length=50, verbose_name='编号', null=True)
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE, verbose_name='关联职员')
    contract = models.ForeignKey(Contract, on_delete=models.CASCADE, verbose_name='对应合同')
    parameter = models.TextField(blank=True, verbose_name='合同对应参数')
    generate = models.TextField(blank=True, verbose_name='生成内容')
    start_data = models.DateField(null=True, verbose_name='开始日期')
    end_data = models.DateField(null=True, verbose_name='结束日期')
    is_valid = models.BooleanField(default=True, verbose_name='是否有效')
    status = models.CharField(default='未处理', max_length=50, verbose_name='状态', null=True)
    is_active = models.BooleanField(default=True, verbose_name='激活')
    vcode = models.CharField(max_length=5, verbose_name='短信验证码', null=True)
    sms_time = models.DateTimeField(verbose_name='短信发送时间', null=True)

    objects = models.Manager()


class ContractManageStatistics(models.Model):
    """每日合同数量统计"""
    date = models.DateField(auto_created=True, auto_now_add=True, verbose_name='日期', unique=True, )
    nums = models.IntegerField(default=0, verbose_name='统计数目')

    objects = models.Manager()
