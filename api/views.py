# Create your views here.

import magic
import random

from django.http import HttpResponse
from django.utils.timezone import now

from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status, generics
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.authtoken.views import ObtainAuthToken

import pdfkit
from benli.juhe import send_sms

from api.models import (
    StaffMember, Area, Household, SocialSecurity, ProvidentFund,
    Contract, Enterprise, Employee, ContractManage
)

from api.serializers import (
    Backend_AuthTokenSerializer, Enterprise_AuthTokenSerializer, Employee_AuthTokenSerializer
)

from api.serializers import (
    StaffMemberSerializer, AreaSerializer, AreaAllSerializer, HouseholdSerializer, HouseholdAllSerializer,
    SocialSecuritySerializer, ProvidentFundSerializer,
    ContractSerializer, ContractListSerializer, Contract_ALL_ListSerializer,
    EnterpriseSerializer, EnterpriseAllSerializer, EmployeeSerializer, EmployeeAllSerializer,
    ContractManageSerializer
)

from benli.qiniu import q, BUCKET_NAME, domain
from qiniu import put_data


class Backend_ObtainAuthToken(ObtainAuthToken):
    """后台登录"""
    serializer_class = Backend_AuthTokenSerializer


class Enterprise_ObtainAuthToken(ObtainAuthToken):
    """企业登录"""
    serializer_class = Enterprise_AuthTokenSerializer


class Employee_ObtainAuthToken(ObtainAuthToken):
    """企业员工登录"""
    serializer_class = Employee_AuthTokenSerializer


class Logout(APIView):
    """注销登录"""

    def get(self, request, format=None):
        request.user.auth_token.delete()
        return Response(status=status.HTTP_200_OK)



class AuthAPIView(APIView):
    "Authorization:'Token f23875345ccdb90902f9b63f9f9ba4c89b488bba'"
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)
    permission_classes = [IsAuthenticated, ]

class Change_Pwd(AuthAPIView, generics.GenericAPIView):
    """修改密码"""

    def post(self, request, *args, **kwargs):
        if request.data.get('old_password', None):
            # 判断旧密码是否正确
            if request.user.check_password(
                    request.data.get('old_password')
            ):
                # 修改密码并保存
                request.user.set_password(
                    request.data.get('new_password')
                )
                request.user.save()
                return Response(True, status=status.HTTP_200_OK)
            else:
                return Response('old password error', status=status.HTTP_401_UNAUTHORIZED)
                # return exceptions.ValidationError(detail='old password error')
        else:
            return Response('need old password', status=status.HTTP_401_UNAUTHORIZED)
            # return exceptions.ValidationError(detail='need old password')

class Backend_LoginInfo(AuthAPIView, generics.ListAPIView):
    """后台登录职员信息"""
    queryset = StaffMember.objects.all()
    serializer_class = StaffMemberSerializer
    pagination_class = None

    def get_queryset(self):
        return self.queryset.filter(
            user=self.request.user
        )


class Enterprise_LoginInfo(AuthAPIView, generics.ListAPIView):
    """企业登录信息"""
    queryset = Enterprise.objects.all()
    serializer_class = EnterpriseSerializer
    pagination_class = None

    def get_queryset(self):
        return self.queryset.filter(
            user=self.request.user
        )


class Employee_LoginInfo(AuthAPIView, generics.ListAPIView):
    """企业员工登录信息"""
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer
    pagination_class = None

    def get_queryset(self):
        return self.queryset.filter(
            user=self.request.user
        )


class StaffMember_ListCreateView(AuthAPIView, generics.ListCreateAPIView):
    """职员列表 创建"""
    queryset = StaffMember.objects.all()
    serializer_class = StaffMemberSerializer

    def get_queryset(self):
        queryset = self.queryset

        if 'is_active' in self.request.query_params.keys():
            if self.request.query_params.get('is_active') == 'true':
                queryset = queryset.filter(
                    user__is_active=True
                )
            elif self.request.query_params.get('is_active') == 'false':
                queryset = queryset.filter(
                    user__is_active=False
                )

        return queryset


class StaffMember_RetrieveUpdateView(AuthAPIView, generics.RetrieveUpdateAPIView):
    """职员更新 禁用激活 修改密码 信息获取"""
    queryset = StaffMember.objects.all()
    serializer_class = StaffMemberSerializer


class Area_AllListView(AuthAPIView, generics.ListAPIView):
    """所有区域列表"""
    queryset = Area.objects.all()
    serializer_class = AreaAllSerializer
    pagination_class = None


class Household_AllListView(AuthAPIView, generics.ListAPIView):
    """所有户口列表"""
    queryset = Household.objects.all()
    serializer_class = HouseholdAllSerializer
    pagination_class = None


class Area_ListCreateView(AuthAPIView, generics.ListCreateAPIView):
    """地区列表 创建"""
    queryset = Area.objects.all()
    serializer_class = AreaSerializer

    def get_queryset(self):
        queryset = self.queryset

        if 'is_active' in self.request.query_params.keys():
            if self.request.query_params.get('is_active') == 'true':
                queryset = queryset.filter(
                    is_active=True
                )
            elif self.request.query_params.get('is_active') == 'false':
                queryset = queryset.filter(
                    is_active=False
                )

        return queryset


class Area_RetrieveUpdateView(AuthAPIView, generics.RetrieveUpdateAPIView):
    """地区更新 禁用激活  信息获取"""
    queryset = Area.objects.all()
    serializer_class = AreaSerializer


class Household_ListCreateView(AuthAPIView, generics.ListCreateAPIView):
    """户口列表 创建"""
    queryset = Household.objects.all()
    serializer_class = HouseholdSerializer

    def get_queryset(self):
        queryset = self.queryset

        if 'is_active' in self.request.query_params.keys():
            if self.request.query_params.get('is_active') == 'true':
                queryset = queryset.filter(
                    is_active=True
                )
            elif self.request.query_params.get('is_active') == 'false':
                queryset = queryset.filter(
                    is_active=False
                )

        return queryset


class Household_RetrieveUpdateView(AuthAPIView, generics.RetrieveUpdateAPIView):
    """户口更新 禁用激活  信息获取"""
    queryset = Household.objects.all()
    serializer_class = HouseholdSerializer


class SocialSecurity_ListCreateView(AuthAPIView, generics.ListCreateAPIView):
    """社保列表 创建"""
    queryset = SocialSecurity.objects.all()
    serializer_class = SocialSecuritySerializer

    def get_queryset(self):
        queryset = self.queryset

        if 'is_active' in self.request.query_params.keys():
            if self.request.query_params.get('is_active') == 'true':
                queryset = queryset.filter(
                    is_active=True
                )
            elif self.request.query_params.get('is_active') == 'false':
                queryset = queryset.filter(
                    is_active=False
                )

        if 'area' in self.request.query_params.keys():
            queryset = queryset.filter(
                area=int(
                    self.request.query_params.get('area')
                )
            )
        return queryset


class SocialSecurity_RetrieveUpdateView(AuthAPIView, generics.RetrieveUpdateAPIView):
    """社保更新 禁用激活  信息获取"""
    queryset = SocialSecurity.objects.all()
    serializer_class = SocialSecuritySerializer


class ProvidentFund_ListCreateView(AuthAPIView, generics.ListCreateAPIView):
    """公积金列表 创建"""
    queryset = ProvidentFund.objects.all()
    serializer_class = ProvidentFundSerializer

    def get_queryset(self):
        queryset = self.queryset

        if 'is_active' in self.request.query_params.keys():
            if self.request.query_params.get('is_active') == 'true':
                queryset = queryset.filter(
                    is_active=True
                )
            elif self.request.query_params.get('is_active') == 'false':
                queryset = queryset.filter(
                    is_active=False
                )

        if 'area' in self.request.query_params.keys():
            queryset = queryset.filter(
                area=int(
                    self.request.query_params.get('area')
                )
            )
        return queryset


class ProvidentFund_RetrieveUpdateView(AuthAPIView, generics.RetrieveUpdateAPIView):
    """公积金更新 禁用激活  信息获取"""
    queryset = ProvidentFund.objects.all()
    serializer_class = ProvidentFundSerializer


class Contract_ListCreateView(AuthAPIView, generics.ListCreateAPIView):
    """合同列表 创建"""
    queryset = Contract.objects.all()
    serializer_class = ContractSerializer

    def get_queryset(self):
        queryset = self.queryset

        if 'is_active' in self.request.query_params.keys():
            if self.request.query_params.get('is_active') == 'true':
                queryset = queryset.filter(
                    is_active=True
                )
            elif self.request.query_params.get('is_active') == 'false':
                queryset = queryset.filter(
                    is_active=False
                )

        if 'area' in self.request.query_params.keys():
            queryset = queryset.filter(
                area=int(
                    self.request.query_params.get('area')
                )
            )

        if 'title' in self.request.query_params.keys():
            queryset = queryset.filter(
                title=self.request.query_params.get('title')
            )

        return queryset


class Contract_All_ListView(AuthAPIView, generics.ListAPIView):
    """合同全部列表"""
    queryset = Contract.objects.all()
    serializer_class = Contract_ALL_ListSerializer
    pagination_class = None

    def get_queryset(self):
        queryset = self.queryset

        if 'is_active' in self.request.query_params.keys():
            if self.request.query_params.get('is_active') == 'true':
                queryset = queryset.filter(
                    is_active=True
                )
            elif self.request.query_params.get('is_active') == 'false':
                queryset = queryset.filter(
                    is_active=False
                )
        else:
            queryset = queryset.filter(
                is_active=True
            )

        if 'area' in self.request.query_params.keys():
            queryset = queryset.filter(
                area=int(
                    self.request.query_params.get('area')
                )
            )

        if 'title' in self.request.query_params.keys():
            queryset = queryset.filter(
                title=self.request.query_params.get('title')
            )

        return queryset


class Contract_RetrieveUpdateView(AuthAPIView, generics.RetrieveUpdateAPIView):
    """合同更新 禁用激活  信息获取"""
    queryset = Contract.objects.all()
    serializer_class = ContractSerializer

class Enterprise_AllListView(AuthAPIView, generics.ListAPIView):
    """所有企业列表"""
    queryset = Enterprise.objects.all()
    serializer_class = EnterpriseAllSerializer
    pagination_class = None

class Enterprise_CreateView(generics.CreateAPIView):
    """企业创建"""
    queryset = Enterprise.objects.all()
    serializer_class = EnterpriseSerializer


class Enterprise_ListView(AuthAPIView, generics.ListAPIView):
    """企业列表"""
    queryset = Enterprise.objects.all()
    serializer_class = EnterpriseSerializer

    def get_queryset(self):
        queryset = self.queryset

        if 'is_active' in self.request.query_params.keys():
            if self.request.query_params.get('is_active') == 'true':
                queryset = queryset.filter(
                    is_active=True
                )
            elif self.request.query_params.get('is_active') == 'false':
                queryset = queryset.filter(
                    is_active=False
                )

        return queryset


class Enterprise_RetrieveUpdateView(AuthAPIView, generics.RetrieveUpdateAPIView):
    """企业更新 禁用激活 修改密码 信息获取"""
    queryset = Enterprise.objects.all()
    serializer_class = EnterpriseSerializer


class Employee_AllListView(AuthAPIView, generics.ListAPIView):
    """所有企业员工列表"""
    queryset = Employee.objects.all()
    serializer_class = EmployeeAllSerializer
    pagination_class = None

    def get_queryset(self):
        queryset = self.queryset

        if 'is_active' in self.request.query_params.keys():
            if self.request.query_params.get('is_active') == 'true':
                queryset = queryset.filter(
                    user__is_active=True
                )
            elif self.request.query_params.get('is_active') == 'false':
                queryset = queryset.filter(
                    user__is_active=False
                )

        if 'enterprise' in self.request.query_params.keys():
            queryset = queryset.filter(
                enterprise=int(
                    self.request.query_params.get('enterprise')
                )
            )

        return queryset

class Employee_ListCreateView(AuthAPIView, generics.ListCreateAPIView):
    """企业员工列表 创建"""
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer

    def get_queryset(self):
        queryset = self.queryset

        if getattr(self.request.user, 'business', None):
            queryset = self.queryset.filter(
                enterprise=self.request.user.business
            )

        if 'is_active' in self.request.query_params.keys():
            if self.request.query_params.get('is_active') == 'true':
                queryset = queryset.filter(
                    user__is_active=True
                )
            elif self.request.query_params.get('is_active') == 'false':
                queryset = queryset.filter(
                    user__is_active=False
                )

        if 'enterprise' in self.request.query_params.keys():
            queryset = queryset.filter(
                enterprise=int(
                    self.request.query_params.get('enterprise')
                )
            )

        return queryset





class Employee_RetrieveUpdateView(AuthAPIView, generics.RetrieveUpdateAPIView):
    """企业员工更新 禁用激活 修改密码 信息获取"""
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer


class ContractManage_ListCreateView(AuthAPIView, generics.ListCreateAPIView):
    """合同管理列表 创建"""
    queryset = ContractManage.objects.all()
    serializer_class = ContractManageSerializer

    def get_queryset(self):
        queryset = self.queryset

        if getattr(self.request.user, 'account', None):
            queryset = self.queryset.filter(
                employee=self.request.user.account
            )
        elif getattr(self.request.user, 'business', None):
            queryset = self.queryset.filter(
                employee__enterprise=self.request.user.business
            )

        if 'enterprise' in self.request.query_params.keys():
            queryset = self.queryset.filter(
                employee__enterprise=int(
                    self.request.query_params.get('enterprise')
                )
            )

        if 'employee' in self.request.query_params.keys():
            queryset = self.queryset.filter(
                employee=int(
                    self.request.query_params.get('employee')
                )
            )

        if 'status' in self.request.query_params.keys():
            queryset = queryset.filter(
                status=self.request.query_params.get('status')
            )

        if 'is_active' in self.request.query_params.keys():
            if self.request.query_params.get('is_active') == 'true':
                queryset = queryset.filter(
                    is_active=True
                )
            elif self.request.query_params.get('is_active') == 'false':
                queryset = queryset.filter(
                    is_active=False
                )

        return queryset


class ContractManage_RetrieveUpdateView(AuthAPIView, generics.RetrieveUpdateAPIView):
    """合同管理更新 禁用激活 修改密码 信息获取"""
    queryset = ContractManage.objects.all()
    serializer_class = ContractManageSerializer


class ContractManage_PDFView(generics.GenericAPIView):
    """合同PDF预览"""
    queryset = ContractManage.objects.all()
    serializer_class = ContractManageSerializer

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="%s.pdf"' % instance.contract.title

        # PDF样式配置
        options = {
            # 'page-size': 'Letter',
            # 'margin-top': '0.75in',
            # 'margin-right': '0.75in',
            # 'margin-bottom': '0.75in',
            # 'margin-left': '0.75in',
            'encoding': "UTF-8",
            # 'no-outline': None
        }

        pdf = pdfkit.from_string(instance.generate, False, options=options)
        response.write(pdf)
        return response


class SMS_VerificationCode(AuthAPIView, generics.GenericAPIView):
    """短信验证码"""
    queryset = ContractManage.objects.all()
    serializer_class = ContractManageSerializer

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        # 验证短信发送间隔 不能少于一分钟
        if instance.sms_time:
            if (now() - instance.sms_time).seconds < 60:
                return Response("短信发送间隔过于频繁,时间间隔为一分钟,请稍后重试", status=status.HTTP_400_BAD_REQUEST)

        # 生成四位随机数字
        vcode = ''.join(
            str(i)
            for i in random.sample(range(0, 9), 4)
        )

        instance.vcode = vcode
        instance.save()
        # 获取手机号码
        phone = instance.employee.phone
        # 发送短信
        res = send_sms(phone=phone, vcode=vcode)

        if res:
            error_code = res["error_code"]
            if error_code == 0:
                # 成功请求
                instance.sms_time = now()
                instance.save()
                return Response("短信发送成功", status=status.HTTP_200_OK)
            else:
                Response("短信发送失败,请稍后重试", status=status.HTTP_400_BAD_REQUEST)
        else:
            Response("短信发送失败,请稍后重试", status=status.HTTP_400_BAD_REQUEST)


class UploadImage(AuthAPIView):
    """上传图片接口"""
    parser_classes = (FormParser, MultiPartParser,)

    def post(self, *args, **kwargs):
        contracts_images_list = []
        me = magic.Magic(mime=True)
        keys = self.request.FILES.keys()
        key = list(keys)[0]
        for contracts_image in self.request.FILES.getlist(key):

            # mimetype 文件大小
            image_content = contracts_image.read()
            mimetype = me.from_buffer(image_content)
            size = contracts_image.size
            # 上传到七牛
            token = q.upload_token(BUCKET_NAME, None, 3600)
            ret, info = put_data(token, None, image_content)
            del contracts_image

            if info.status_code == 200:
                thumbnai = '?imageMogr2/auto-orient/thumbnail/100x/gravity/Center/crop/x100/blur/1x0/quality/75%7Cimageslim'
                contracts_images_list.append({'name': ret.get('hash', None),
                                              'mimetype': mimetype,
                                              'size': size,
                                              'url': domain + ret.get('hash', None),
                                              'thumbnailUrl': domain + ret.get('hash', None) + thumbnai})

        return Response({key[:-2]: contracts_images_list})
