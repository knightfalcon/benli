# Generated by Django 2.1.2 on 2018-10-21 07:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0012_auto_20181020_1316'),
    ]

    operations = [
        migrations.AddField(
            model_name='contract',
            name='variable',
            field=models.TextField(blank=True, verbose_name='变量'),
        ),
    ]
