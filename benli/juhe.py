
import json
from urllib.parse import urlencode
from urllib.request import urlopen

def send_sms(appkey="973b73a6d3a892e6065c266bc4c44c59",
             tpl_id="112233",
             phone=None,
             vcode=None):

    url = "http://v.juhe.cn/sms/send"
    params = {
        "mobile": phone,  # 接收短信的手机号码
        "tpl_id": tpl_id,  # 短信模板ID，请参考个人中心短信模板设置
        "tpl_value": "#code#=%s" % vcode,
    # 变量名和变量值对。如果你的变量名或者变量值中带有#&amp;=中的任意一个特殊符号，请先分别进行urlencode编码后再传递，&lt;a href=&quot;http://www.juhe.cn/news/index/id/50&quot; target=&quot;_blank&quot;&gt;详细说明&gt;&lt;/a&gt;
        "key": appkey,  # 应用APPKEY(应用详细页查询)
        "dtype": "json",  # 返回数据的格式,xml或json，默认json

    }

    params = urlencode(params)
    f = urlopen("%s?%s" % (url, params))

    content = f.read()
    return json.loads(content)
